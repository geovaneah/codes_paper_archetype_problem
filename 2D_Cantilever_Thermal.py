"""
Created on 5th of May, 2022
by Geovane Augusto Haveroth

Topology optimization using 
* SIMP/RAMP interpolation scheme for the elasticity problem;
* SIMP/RAMP interpolation scheme for heat equation in the sub-problems;
* Isotropic Helmoltz PDE filtering solved by the FEM;
* Volume constraint;
* PUP (see Qian 2017) contraint;
* Grayness constraint;
* Sub-problems defined layer-by-layer: Steady-state heat equation
"""

###############################################################################
###############################################################################
# libraries

import matplotlib.pyplot as plt
from fenics import*
import numpy as np
from MMA import mmasub
import os
import time

###############################################################################
###############################################################################
# setting TO parameters

# penalty for the main problem
penalty_m = Constant(5.0)
# interpolation scheme for the Main problem "SIMP/RAMP"
method_m = "SIMP"
# penalty term for the Subproblems 
penalty_s = Constant(5.0)
# interpolation scheme for the Subproblem "SIMP/RAMP"
method_s = "RAMP"
## threshold projection parameters 
# Perform the projection step: True/False
flag_threshold = True
# fisrt parameter that gives the transition
threshold_eta = 0.5
# initial weight for the transition
threshold_beta = 1
# minimal number of steps to increase threshold_beta
threshold_beta_increment = 100
# maximum value for threshold_beta
threshold_beta_maximum = 32
## algorithm parameters
# total number of iterations
niter = 1000
# tolerance for stop criterium based on the physical density difference
tolerance_density = 1e-2
# saving less informations, jumping save_jump_steps iterations
save_jump_iterations = 5

###############################################################################
###############################################################################
# setting AM constraints and sub-problems parameters

## constraints:
# position 0 is the volume: True/False
# position 1 is the geometric AM (PUP): True/False
# position 2 is the grayness: True/False
what_constraints = [True, False, False]
# number of constraints
number_of_constraints = sum(what_constraints)
## constraint upper limit only when True
gval_upper = []
if (what_constraints[0]):
    gval_upper.append(0.5)
if (what_constraints[1]):
    gval_upper.append(1.0)
if (what_constraints[2]):
    gval_upper.append(1.0)

# build direction
build_direction_angle = np.pi/2.0
build_direction = np.array([np.cos(build_direction_angle), np.sin(build_direction_angle)])
# overhang angle
overhang_angle = np.pi/4.0
# defining the number of subproblems (= number of meshes)
number_of_subproblems = 40
# minimal thermal conductivity
delta_conductivity = Constant(1e-9)
# weight of each sub-problem
weight_reference = 0.25; weight = []
for subproblem in range(0, number_of_subproblems):
    weight.append((1./weight_reference-1.)/number_of_subproblems)

###############################################################################
###############################################################################
# defining mesh, load and material properties

# size and discretization (unit thickness)
Lx = 12.; Ly = 6.; Lz = 1.; Nx = 240; Ny = 120; Nz = 1
# list of meshes
mesh = []
for subproblem in range(0, number_of_subproblems):
    Lyn = (number_of_subproblems-subproblem)*Ly/number_of_subproblems
    Nyn = (number_of_subproblems-subproblem)*Ny/number_of_subproblems
    mesh.append(RectangleMesh.create([Point(0.0,0.0), Point(Lx, Lyn)], [Nx, int(Nyn)], CellType.Type.quadrilateral))
# numerical domain volume of mesh[0]
numdomain_volume = assemble(Constant(1)*dx(mesh[0]))
## loading
# concentrated load
load_concentrated_intensity = np.array([0.0, -1.0])
load_concentrated_position = Point(Lx, Ly/2.)
## material parameters
# Young modulus
E0 = Constant(1)
# Minimum Young modulus
Emin = Constant(1e-9)
# Poisson ratio
nu = Constant(0.3)
 
###############################################################################
############################################################################### 
# Radius for the Helmholtz PDE filtering
filter_radius_rbar = 1.25

###############################################################################
###############################################################################
# Create directory and files to save the results

sfile = 'Thermal'
sfile += '_volume_'+str(gval_upper[0])
sfile += '_'+str(Lx)+'x'+str(Ly)+'x'+str(Lz)
sfile += '_'+str(Nx)+'x'+str(Ny)+'x'+str(Nz)
sfile += '_penaltym_'+str(float(penalty_m))
sfile += '_penaltys_'+str(float(penalty_s))
sfile += '_layers_'+str(number_of_subproblems)
sfile += '_weight_'+str(weight_reference)
sfile += '_filter_radius_'+str(filter_radius_rbar)
sfile += '_element_'+str(mesh[0].ufl_cell())
if os.path.exists(sfile) == False:
    os.makedirs(sfile)
ofile = open(sfile+'/out_file.dat', "w")
ffile = XDMFFile(sfile+'/topology_optimization.xdmf')
ffile.parameters["flush_output"]=True
ffile.parameters["functions_share_mesh"]=True

###############################################################################
###############################################################################
# defining the function spaces and functions

space_CG1 = []; trial_CG1 = []; test_CG1 = [];
temperature_CG1 = []; density_CG1 = []; dofmap_mesh_CG1 = []; pos_mesh_CG1 = [];
space_CGV1 = []; trial_CGV1 = []; test_CGV1 = []; u_CGV1 = [];
for subproblem in range(0, number_of_subproblems):
    # list of function spaces: Continuous Galerkin of degree 1
    space_CG1.append(FunctionSpace(mesh[subproblem], "CG", 1))
    # list of trial functions for CG1
    trial_CG1.append(TrialFunction(space_CG1[subproblem]))
    # list of test functions for CG1
    test_CG1.append(TestFunction(space_CG1[subproblem]))
    # list of functions density_CG1
    density_CG1.append(Function(space_CG1[subproblem], name="Nodal density"))
    # list of functions temperature_CG1
    temperature_CG1.append(Function(space_CG1[subproblem]))
    # list of dof mappings
    dofmap_mesh_CG1.append(vertex_to_dof_map(space_CG1[subproblem]))
    # list of positions
    pos_mesh_CG1.append(np.arange(0, mesh[subproblem].num_vertices()))
    # list of function spaces: Continuous Galerkin of degree 1
    space_CGV1.append(VectorFunctionSpace(mesh[subproblem], "CG", 1))
    # list of trial functions for CGV1
    trial_CGV1.append(TrialFunction(space_CGV1[subproblem]))
    # list of test functions for CGV1
    test_CGV1.append(TestFunction(space_CGV1[subproblem]))
    # list of displacement functions  
    u_CGV1.append(Function(space_CGV1[subproblem], name="Displacement"))
# density function to the smooth density version after Helmholtz filtering
density_Helmholtz_CG1 = Function(space_CG1[0], name="Helmholtz density")
# density function to the projected density version after the threshold
density_Threshold_CG1 = Function(space_CG1[0], name="Threshold density")
# old density function for the stop criteria
density_Old_CG1 = Function(space_CG1[0], name="Old design density")
# sensitivity for the objective function
df = Function(space_CG1[0], name="Objective sensitivity")
# sensitivity for the constraints
dg = []
for constraint in range(0, number_of_constraints):
    dg.append(Function(space_CG1[0], name="Constraint sensitivity"))
# auxiliar function for the filtering solving
aux_function_CG1 = Function(space_CG1[0], name="Auxiliar function in CG1")

###############################################################################
###############################################################################
# defs

# defining stress tensor
def sigma(w, s, penal, method):
    if (method == 'SIMP'):
        factor = s**penal
    elif (method == 'RAMP'):
        factor = s/(1.+penal*(1.-s))
    E = Emin+factor*(E0-Emin)
    mu = E/(2.*(1.+nu))
    lamda = (E*nu/(1.+nu))/(1.-2.*nu)
    return 2.*mu*epsilon(w)+lamda*div(w)*Identity(mesh[0].topology().dim())

# defining strain tensor
def epsilon(w):
    return sym(grad(w))

# set clamped boundary
def clamped_boundary(x, on_boundary):
    return near(x[0], 0.0, 1e-6) and on_boundary

# set build plate boundary
def bottom_boundary(x, on_boundary):
    return near(x[1], 0.0, 1e-6) and on_boundary

# set upper boundaries
def top_boundary(x, on_boundary):
    return near(x[1], Lyn, 1e-6) and on_boundary

# def to find and apply the concentrated loads at the nodes
def ApplyLoadAtRhs(Rhs_motion, load_concentrated_position, load_concentrated_intensity):
    for vertex in vertices(mesh[0]):
        if (near(vertex.point()[0], load_concentrated_position[0], 1e-6) and near(vertex.point()[1], load_concentrated_position[1], 1e-6)):
            Rhs_motion[space_CGV1[0].dofmap().entity_dofs(mesh[0], 0, [vertex.index()])] = load_concentrated_intensity
            break
    return

# set elasticity solver
def elasticity_solver():
    Lhs_motion = assemble(a_motion)
    bc_motion.apply(Lhs_motion)
    solve(Lhs_motion, u_CGV1[0].vector(), Rhs_motion, "mumps")
    return

###############################################################################
###############################################################################
# formulating the variational problems

# defining the main problem
a_motion = inner(sigma(trial_CGV1[0], density_CG1[0], penalty_m, method_m), epsilon(test_CGV1[0]))*dx(mesh[0])
L_motion = dot(Constant((0.,0.)), test_CGV1[0])*dx(mesh[0])
bc_motion = DirichletBC(space_CGV1[0], Constant((0,0)), clamped_boundary)
Rhs_motion = assemble(L_motion)
ApplyLoadAtRhs(Rhs_motion, load_concentrated_position, load_concentrated_intensity)
bc_motion.apply(Rhs_motion)

# defining the subproblems
Ks = []; a_subproblems_heat = []; L_subproblems_heat = []; bc_subproblems_heat = []; top_ds = []
heat_subproblem = []; heat_subproblem_solver = []
for subproblem in range(0, number_of_subproblems):
    Lyn = (number_of_subproblems-subproblem)*Ly/number_of_subproblems
    load_marker = MeshFunction("size_t", mesh[subproblem], mesh[0].topology().dim()-1)
    AutoSubDomain(top_boundary).mark(load_marker, 2)
    top_ds.append(Measure("ds", subdomain_data=load_marker))
    if (method_s == 'SIMP'):
        factor = density_CG1[subproblem]**penalty_s
    elif (method_s == 'RAMP'):
        factor = density_CG1[subproblem]/(1.+penalty_s*(1.-density_CG1[subproblem]))
    Ks.append(delta_conductivity+(1.-delta_conductivity)*factor)
    a_subproblems_heat.append(inner(Ks[subproblem]*grad(trial_CG1[subproblem]), grad(test_CG1[subproblem]))*dx(mesh[subproblem]))
    L_subproblems_heat.append(density_CG1[subproblem]*test_CG1[subproblem]*top_ds[subproblem](2))
    bc_subproblems_heat.append(DirichletBC(space_CG1[subproblem], Constant(0), bottom_boundary))
    heat_subproblem.append(LinearVariationalProblem(a_subproblems_heat[subproblem], L_subproblems_heat[subproblem], temperature_CG1[subproblem], bc_subproblems_heat[subproblem]))
    heat_subproblem_solver.append(LinearVariationalSolver(heat_subproblem[subproblem]))
    heat_subproblem_solver[subproblem].parameters["linear_solver"]= "mumps"

# defining the Helmholtz filtering PDE problem
filter_radius_r = filter_radius_rbar/(2.*sqrt(3.))
a_Helmholtz = (filter_radius_r**2)*inner(grad(trial_CG1[0]), grad(test_CG1[0]))*dx+dot(trial_CG1[0], test_CG1[0])*dx(mesh[0])
L_Helmholtz = dot(density_CG1[0], test_CG1[0])*dx(mesh[0])
M_Helmholtz = assemble(dot(trial_CG1[0], test_CG1[0])*dx(mesh[0]))
K_Helmholtz = assemble(a_Helmholtz)
Helmholtz_solver = LUSolver(K_Helmholtz)

###############################################################################
###############################################################################
# MMA solver initialization

# Column vector with the lower bounds for the variables x_j.
density_CG1_math_min = np.zeros(mesh[0].num_vertices())[np.newaxis].T
# Column vector with the upper bounds for the variables x_j
density_CG1_math_max = np.ones(mesh[0].num_vertices())[np.newaxis].T
# Column vector with the current values of the variables x_j 
density_CG1_math = gval_upper[0]*np.ones(mesh[0].num_vertices())
# xval, one iteration ago (provided that iter>1). 
density_CG1_math_old1 = density_CG1_math
# xval, two iterations ago (provided that iter>2).
density_CG1_math_old2 = density_CG1_math
# Column vector with the lower asymptotes from the previous iteration (provided that iter>1).
lowmma = np.ones(mesh[0].num_vertices())[np.newaxis].T 
# Column vector with the upper asymptotes from the previous iteration (provided that iter>1).
upmma = np.ones(mesh[0].num_vertices())[np.newaxis].T

###############################################################################
###############################################################################
# optimization procedure

# initialize some useful variables
n, change_density, threshold_beta_counter  = 0, 1., 0
total_cost_history = [1e+30]
compliance_history = [1e+30]
constraint_history = []
gray_level_history = [100]
start = time.time()

# the optimization procedure
while change_density > tolerance_density and n < niter:
       
    # iteration increment
    n += 1
      
    # save the last physical density field
    density_Old_CG1.assign(density_CG1[0])
    
    # get physical density variable
    density_CG1[0].vector()[:] = density_CG1_math
     
    # density filtering to modify the physical variable
    # filter procedure
    b_Helmholtz = assemble(L_Helmholtz)
    Helmholtz_solver.solve(density_CG1[0].vector(), b_Helmholtz)
    density_Helmholtz_CG1.vector()[:] = np.minimum(np.maximum(density_CG1[0].vector()[:], 0.), 1.)
    density_CG1[0].assign(density_Helmholtz_CG1)
    # threshold procedure
    if (flag_threshold):
        threshold_auxiliar_a = np.tanh(threshold_beta*threshold_eta)+np.tanh(threshold_beta*(density_Helmholtz_CG1.vector()[:]-threshold_eta))
        threshold_auxiliar_b = np.tanh(threshold_beta*threshold_eta)+np.tanh(threshold_beta*(1.-threshold_eta))
        density_Threshold_CG1.vector()[:] = threshold_auxiliar_a/threshold_auxiliar_b
        density_CG1[0].assign(density_Threshold_CG1)

    # compute solution of the elasticity main problem
    elasticity_solver()
    
    # compliance and the constraints in the variational form
    f_variational = inner(sigma(u_CGV1[0], density_CG1[0], penalty_m, method_m), epsilon(u_CGV1[0]))*dx(mesh[0])
    g_variational = []
    if (what_constraints[0]):
        g_variational.append(density_CG1[0]/numdomain_volume*dx(mesh[0]))
    if (what_constraints[1]):
        xi_value = inner(grad(density_CG1[0])/sqrt(inner(grad(density_CG1[0]), grad(density_CG1[0]))+1e-9), Constant(build_direction))-cos(overhang_angle)
        g_variational.append(inner(grad(density_CG1[0]), Constant(build_direction))/(1.+exp(-2.*10.*xi_value))*dx(mesh[0]))
    if (what_constraints[2]):
        g_variational.append(4.*density_CG1[0]*(1.-density_CG1[0])/numdomain_volume*dx(mesh[0]))
    
    # get fval and gval values
    fval = assemble(f_variational)
    gval = []
    for constraint in range(0, number_of_constraints):
        gval.append(assemble(g_variational[constraint])-gval_upper[constraint])
        
    # save fval (main problem) and gval
    compliance_history.append(fval)
    constraint_history.append([gval[i]+gval_upper[i] for i in range(0, len(gval))])

    # get sensitivity with respect to the physical variable
    # adjoint parameter
    lm_CGV1 = -2.*u_CGV1[0]
    # sensitivity of the main compliance based on the lm in the variational form
    f_variational += inner(sigma(u_CGV1[0], density_CG1[0], penalty_m, method_m), epsilon(lm_CGV1))*dx(mesh[0])
    df.vector()[:] = assemble(derivative(f_variational, density_CG1[0]))
    for constraint in range(0, number_of_constraints):
        dg[constraint].vector()[:] = assemble(derivative(g_variational[constraint], density_CG1[0]))
    
    # loop over subproblems
    for subproblem in range(0, number_of_subproblems):
        # get the current density in the subproblem mesh
        density_CG1[subproblem].vector()[dofmap_mesh_CG1[subproblem]] = density_CG1[0].vector()[dofmap_mesh_CG1[0][pos_mesh_CG1[subproblem]]]
        # solve the current subproblem
        heat_subproblem_solver[subproblem].solve()
        # set the subproblem compliance in the variational form
        f_subproblem_variational = weight[subproblem]*inner(Ks[subproblem]*grad(temperature_CG1[subproblem]), grad(temperature_CG1[subproblem]))*dx(mesh[subproblem]);
        # evaluate the compliance
        fval += assemble(f_subproblem_variational)
        # compute the adjoint parameter
        lm_CG1 = -2.*weight[subproblem]*temperature_CG1[subproblem]
        # sensitivity based on the lm in the variational form
        f_subproblem_variational += inner(Ks[subproblem]*grad(temperature_CG1[subproblem]), grad(lm_CG1))*dx(mesh[subproblem])
        f_subproblem_variational -= density_CG1[subproblem]*lm_CG1*top_ds[subproblem](2)
        # compute and add the compliance sensitivity
        df.vector()[dofmap_mesh_CG1[0][pos_mesh_CG1[subproblem]]] += assemble(derivative(f_subproblem_variational, density_CG1[subproblem]))[dofmap_mesh_CG1[subproblem]]
    
    # save the compliance (sum of the several terms)
    total_cost_history.append(fval)
    
    ## sensitivity corrections
    # threshold
    if (flag_threshold):
        threshold_auxiliar_b = np.tanh(threshold_beta*threshold_eta)+np.tanh(threshold_beta*(1.-threshold_eta))
        threshold_auxiliar_c = np.cosh(threshold_beta*(density_Helmholtz_CG1.vector()[:]-threshold_eta))**2.
        threshold_auxiliar_d = threshold_beta/threshold_auxiliar_b
        df.vector()[:] = threshold_auxiliar_d*np.divide(df.vector()[:], threshold_auxiliar_c)
        for constraint in range(0, number_of_constraints):
            dg[constraint].vector()[:] = threshold_auxiliar_d*np.divide(dg[constraint].vector()[:], threshold_auxiliar_c)
    # filtering
    Helmholtz_solver.solve(aux_function_CG1.vector(), df.vector())
    df.vector().set_local(M_Helmholtz*aux_function_CG1.vector())
    for constraint in range(0, number_of_constraints):
        Helmholtz_solver.solve(aux_function_CG1.vector(), dg[constraint].vector())
        dg[constraint].vector().set_local(M_Helmholtz*aux_function_CG1.vector())
    
    # Method of moving asymptotes to evaluate the next mathematical density variable
    move = 0.2
    scale = np.ones(number_of_constraints+1); scale[1] *= 100.
    fval = scale[0]*fval
    dfdx = scale[0]*df.vector()[:][np.newaxis].T
    gval = np.array([scale[constraint+1]*gval[constraint] for constraint in range(0, number_of_constraints)])[np.newaxis].T
    dgdx = scale[1]*dg[0].vector()[:][np.newaxis]
    for constraint in range(1, number_of_constraints):
        dgdx = np.concatenate((dgdx, scale[constraint+1]*dg[constraint].vector()[:][np.newaxis]), axis=0)
    xmma,ymma,zmma,lam,xsi,etaa,muu,zeta,ss,lowmma,upmma = \
        mmasub(number_of_constraints,                   # number of general constraints
               mesh[0].num_vertices(),                  # number of variables x_j
               n,                                       # current iteration number
               density_CG1_math[np.newaxis].T,          # current values of the variables x_j
               density_CG1_math_min,                    # lower bounds for the variables x_j
               density_CG1_math_max,                    # upper bounds for the variables x_j
               density_CG1_math_old1[np.newaxis].T,     # xval, one iteration ago
               density_CG1_math_old2[np.newaxis].T,     # xval, two iterations ago
               fval,    # value of the objective function at xval
               dfdx,    # derivatives of the objective function with respect to the variables x_j, calculated at xval.
               gval,    # values of the constraint functions, calculated at xval.
               dgdx,    # derivatives of the constraint functions with respect to the variables x_j, calculated at xval.
               lowmma,  # lower asymptotes from the previous iteration
               upmma,   # upper asymptotes from the previous iteration
               1.0,     # constants a_0 in the term a_0*z.
               np.zeros((number_of_constraints, 1)),        # constants a_i in the terms a_i*z
               10000*np.ones((number_of_constraints, 1)),   # constants c_i in the terms c_i*y_i
               np.zeros((number_of_constraints, 1)),        # constants d_i in the terms 0.5*d_i*(y_i)^2
               move)
    density_CG1_math_old2 = density_CG1_math_old1
    density_CG1_math_old1 = density_CG1_math
    density_CG1_math = xmma.copy().flatten().T
    
    # stop criteria infinity norm of the relative density difference
    change_density = np.linalg.norm(density_CG1[0].vector()[:]-density_Old_CG1.vector()[:], np.inf)
    
    # density average with the physical density variable
    density_average = assemble(density_CG1[0]*dx(mesh[0]))/numdomain_volume
    
    # discreteness measure
    gray_level_history.append(assemble(400.*density_CG1[0]*(1.-density_CG1[0])/numdomain_volume*dx(mesh[0])))
    
    # applying threshold projection: update beta parameter: Generally, it is a heuristic rule
    if (flag_threshold):
        threshold_beta_counter += 1
        if ((threshold_beta_counter == threshold_beta_increment or change_density < tolerance_density) and threshold_beta < threshold_beta_maximum):
            change_density, threshold_beta_counter = 1., 0
            threshold_beta *= 2.0
        
    # write less information jumping save_jump_iterations iterations
    if (n%save_jump_iterations == 0):
        ffile.write(density_CG1[0], n)
        ffile.write(u_CGV1[0], n) 
        
    # print some results in the screen
    print("Ite: {0}, Den:{1:.6e}, C: {2:.6e}, Change: {3:.6e}".format(n, density_average, total_cost_history[n], change_density))

    # print information in the out file
    print("Iteration %d" % n, file=ofile)
    print("Beta threshold projection parameter", threshold_beta, file=ofile)
    print("Compliance", compliance_history[n], file=ofile)
    print("Total cost", total_cost_history[n], file=ofile)
    print("Average density", density_average, file=ofile)
    print("Gray level %", gray_level_history[n], file=ofile)

# plot time
elaspsed_time = time.time() - start
print("Elapsed time:", elaspsed_time)
print("Elapsed time:", elaspsed_time, file=ofile)

# plot figures
fig = plt.figure()
left_axis = fig.add_subplot()
left_axis.set_xlabel("Iterations")
left_axis.set_ylabel("Total cost", color="tab:red")
left_axis.plot(np.arange(10, n+1), total_cost_history[10:n+1], color="tab:red")
left_axis.tick_params(axis='y', labelcolor="tab:red")
right_axis = left_axis.twinx()
right_axis.set_ylabel("Gray level (%)", color="tab:blue")
right_axis.plot(np.arange(10, n+1), gray_level_history[10:n+1], color="tab:blue")
right_axis.tick_params(axis='y', labelcolor="tab:blue")

#fig.tight_layout()
fig.savefig(sfile+'/total_cost_history.png')

# close out file
ofile.close()

# saving optimized structure density
file = open(sfile+"/Optimized_structure_density_math.txt", "w+")
for k in range(0,len(density_CG1_math)):
    content = str(density_CG1_math[k])+'\n'
    file.write(content)
file.close()

# saving optimized structure density
file = open(sfile+"/Optimized_structure_density_physical.txt", "w+")
for k in range(0,len(density_CG1_math)):
    content = str(density_CG1[0].vector()[k])+'\n'
    file.write(content)
file.close()

# saving the compliance (main problem) history
file = open(sfile+"/Compliance_history.txt", "w+")
for k in range(1,len(compliance_history)):
    content = str(k)+'\t'+str(compliance_history[k])+'\n'
    file.write(content)
file.close()

# saving the objective (sum of compliances with weights) history
file = open(sfile+"/total_cost_history.txt", "w+")
for k in range(1,len(total_cost_history)):
    content = str(k)+'\t'+str(total_cost_history[k])+'\n'
    file.write(content)
file.close()

# saving the greyness history
file = open(sfile+"/Grayness_history.txt", "w+")
for k in range(1,len(gray_level_history)):
    content = str(k)+'\t'+str(gray_level_history[k])+'\n'
    file.write(content)
file.close()

# saving the constraint history
file = open(sfile+"/Constraint_history.txt", "w+")
for k in range(0, len(constraint_history)):
    content = str(k+1)+'\t'
    for constraint in range(0, number_of_constraints):
        content += str(constraint_history[k][constraint])
        content += '\t'
    content += '\n'
    file.write(content)
file.close()

