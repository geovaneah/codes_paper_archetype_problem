# Codes for the paper: "Topology Optimization with Additive Manufacturing Physical Constraint:  Exploring the layer by layer process".

## Authors and filliations
Thore, Carl-Johan [1] and Klarbring, Anders [1] and Jakobsson, Stefan [2] and Correa, Maicon Ribeiro [3] and Ausas, Roberto Federico [4] Cuminato, José Alberto [4] and Haveroth, Geovane Augusto [4].

[1] Solid Mechanics, Department of Management and Engineering, Institute of Technology, Linköping University, 581 83 Linköping, Sweden
[2] Arcam EBM, Linköping, Sweden
[3] Department of Applied Mathematics, Institute of Mathematics, Statistics and Scientific Computing, University of Campinas, Campinas-SP, Brazil
[4] Department of Applied Mathematics and Statistics, Institute of Mathematics and Computer Sciences, University of São Paulo, São Carlos-SP, Brazil

## License
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Acknowledgment
The authors would like to thank the São Paulo Research Foundation (FAPESP) and the National Council for Scientific and Technological Development (CNPq) under grants CEPID-CeMEAI 2013/07375-0 and 2020/14288-0.

## Hits
<div id="sfcjbu5lqtx1g1tak4s27gxp2mg38wfzlbx"></div><script type="text/javascript" src="https://counter2.stat.ovh/private/counter.js?c=jbu5lqtx1g1tak4s27gxp2mg38wfzlbx&down=async" async></script><br><a href="https://www.freecounterstat.com"> </a><noscript><a href="https://www.freecounterstat.com" title=""><img src="https://counter2.stat.ovh/private/freecounterstat.php?c=jbu5lqtx1g1tak4s27gxp2mg38wfzlbx" border="0" title="" alt=""></a></noscript>  

           
